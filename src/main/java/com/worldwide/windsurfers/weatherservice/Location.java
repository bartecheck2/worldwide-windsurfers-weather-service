package com.worldwide.windsurfers.weatherservice;

public enum Location {
    JASTARNIA("Jastarnia", "Poland"),
    BRIDGETOWN("Bridgetown", "Barbados"),
    FORTALEZA ("Fortaleza", "Brazil"),
    PISSOURI ("Pissouri","Cyprus"),
    LEMORNE ("Le Morne", "Mauritius");

    Location(String city, String country) {
    }
}
