package com.worldwide.windsurfers.weatherservice;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class WindsurfersService {

    @RequestMapping("/")
    public String index() {
        return "Weather";
    }
}
