package com.worldwide.windsurfers.weatherservice;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.web.client.RestTemplate;

@SpringBootApplication
public class WeatherServiceApplication {

    private static final Logger log = LoggerFactory.getLogger(WeatherServiceApplication.class);

	public static void main(String[] args) {
		SpringApplication.run(WeatherServiceApplication.class, args);
	}

    @Bean
    public RestTemplate restTemplate(RestTemplateBuilder builder) {
        return builder.build();
    }

    @Bean
    public CommandLineRunner run(RestTemplate restTemplate) throws Exception {
        return args -> {
            Object quote = restTemplate.getForObject(
                    "https://api.weatherbit.io/v2.0/forecast/daily?city=Raleigh,NC&key=c045caf1761448659eb5ad155e4b2800", Object.class);
            log.info(quote.toString());
        };
    }
}
